const express = require('express');
const app = express();
const mainRouter = require('./routes');
const bodyParser = require('body-parser');
const cors = require('cors');
const { MongoClient } = require('mongodb');

require('./worker');

app.use(cors());
app.use(bodyParser.json());
app.use('/', mainRouter);

MongoClient.connect(process.env.MONGO_HOST || 'mongodb://localhost', { useNewUrlParser: true }, (err, client) => {
    if (err) throw new Error(err);
    app.locals.db = client.db('distribuidos');
    app.listen(3000, () => {
        console.log('Server running on port 3000...');
    });
});

