const mqtt = require('mqtt');

const client = mqtt.connect(process.env.BROKER_URL);

client.on('connect', () => {
    console.log('Connected to ActiveMQ through MQTT protocol...');
    //Subscribe to interesed topics
    // client.subscribe('/any');
});

client.on('error', (error) => {
    console.error(`MQTT Error: ${error}`);
    throw new Error(error);
});

//Handling message from multiple topics
client.on('message', (topic, message) => {
    console.log(`Message from topic ${topic}: ${message}`);
});

const publish = (topic, message, cb) => {
    if (client.connected === true) client.publish(topic, message, cb);
    else console.log('Client is not connected');
};

module.exports = {
    client,
    publish,
};