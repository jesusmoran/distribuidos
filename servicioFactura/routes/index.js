const router = require('express').Router();
const { publish } = require('../worker');

router.get('/', (req, res, next) => {
    res.send('Service FACTURA running.');
});

router.post('/factura', (req, res, next) => {
    const { factura } = req.body;
    const collection = req.app.locals.db.collection('facturas');
    collection.insertOne(factura, (err, response) => {
        if (err) res.status(500).send({ error: err });
        publish('/factura/registrada', JSON.stringify(factura), (err) => {
            if (err) res.status(500).send({ error: err });
            res.send({ message: 'Message published on ActiveMQ' });
        });
    });
});

module.exports = router;