#!/usr/bin/env python
# coding=utf-8

import os, json, unicodedata
from flask import Flask, request, jsonify
from flask_mqtt import Mqtt
from flask_pymongo import PyMongo

app = Flask(__name__)
app.config['MQTT_BROKER_URL'] = 'localhost'
app.config['MONGO_URI'] = 'mongodb://localhost/distribuidos'
mqtt = Mqtt(app)

# MONGO_HOST = os.getenv('MONGO_HOST', 'localhost')
# MONGO_PORT = os.getenv('MONGO_PORT', 27017)
mongo = PyMongo(app)

@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    mqtt.subscribe('/factura/registrada')

@mqtt.on_message()
def handle_mqtt_message(client, userdata, message):
    payload = message.payload.decode()
    data = json.loads(payload)

    total = 0
    for i in data["items"]:
        total += i["sub"]

    cuentaNueva = {
        'number': data["number"],
        'dni': data["dni"],
        'total': total
    }
    uni = json.dumps(cuentaNueva)
    mongo.db.cuentas.insert(cuentaNueva)
    mqtt.publish('/cuenta/nueva', uni)
    

@app.route('/')
def message():
    return jsonify(message = 'ping')